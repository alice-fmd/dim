dnl -*- mode: Autoconf -*- 
dnl
dnl AC_ROOT_DIM([MINIMUM-VERSION 
dnl                   [,ACTION-IF_FOUND 
dnl                    [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_ROOT_DIM],
[
    AC_ARG_WITH([dim-prefix],
        [AC_HELP_STRING([--with-dim-prefix],
		[Prefix where Dim is installed])],
        dim_prefix=$withval, dim_prefix="")

    if test "x${DIM_CONFIG+set}" != xset ; then 
        if test "x$dim_prefix" != "x" ; then 
	    DIM_CONFIG=$dim_prefix/bin/dim-config
	fi
    fi   
    if test "x${DIM_CONFIG+set}" != xset ; then 
        if test "x$dim_prefix" != "x" ; then 
	    DIM_CONFIG=$dim_prefix/bin/dim-config
	fi
    fi   
    AC_PATH_PROG(DIM_CONFIG, dim-config, no)
    dim_min_version=ifelse([$1], ,0.1,$1)
    
    AC_MSG_CHECKING(for Dim version >= $dim_min_version)

    dim_found=no    
    if test "x$DIM_CONFIG" != "xno" ; then 
       DIM_CFLAGS=`$DIM_CONFIG --cflags`
       DIM_CPPFLAGS=`$DIM_CONFIG --cppflags`
       DIM_INCLUDEDIR=`$DIM_CONFIG --includedir`
       DIM_LIBS=`$DIM_CONFIG --libs`
       DIM_LIBDIR=`$DIM_CONFIG --libdir`
       DIM_LDFLAGS=`$DIM_CONFIG --ldflags`
       DIM_PREFIX=`$DIM_CONFIG --prefix`
       DIM_JNILIBS=`$DIM_CONFIG --libs`
       
       dim_version=`$DIM_CONFIG -V` 
       dim_vers=`echo $dim_version | \
         awk 'BEGIN { FS = " "; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       dim_regu=`echo $dim_min_version | \
         awk 'BEGIN { FS = " "; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $dim_vers -ge $dim_regu ; then 
            dim_found=yes
       fi
    fi
    AC_MSG_RESULT($dim_found - is $dim_version) 
  
    if test "x$dim_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(DIM_PREFIX)
    AC_SUBST(DIM_CFLAGS)
    AC_SUBST(DIM_CPPFLAGS)
    AC_SUBST(DIM_INCLUDEDIR)
    AC_SUBST(DIM_LDFLAGS)
    AC_SUBST(DIM_LIBDIR)
    AC_SUBST(DIM_LIBS)
    AC_SUBST(DIM_JNILIBS)
])

dnl
dnl EOF
dnl 
