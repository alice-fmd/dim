#!/bin/sh
### BEGIN INIT INFO
# Provides:          dim-dns
# Required-Start:    $syslog $local_fs $remote_fs
# Required-Stop:     $syslog $local_fs $remote_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: DIM Domain Name Server
# Description:       Init script for Distrbuted Information Management's 
#                    Domain Name Server. 
### END INIT INFO
#
# Author:	Christian Holm Chistensen <cholm@nbi.dk>
#
# set -e
prefix=/usr
exec_prefix=/usr
sbindir=/usr/sbin
localstatedir=/var
sysconfdir=/etc

PATH=/bin:/usr/bin:/sbin:/usr/sbin:${sbindir}
DAEMON=${sbindir}/dim_dns
NAME="dim-dns"
DESC="Distributed Information Manager Domain Name Server"
PIDFILE=${localstatedir}/run/$NAME.pid
LOGFILE=${localstatedir}/log/$NAME


test -x $DAEMON || exit 0

# Load the VERBOSE setting and other rcS variables
. /lib/init/vars.sh

# We use LSB init scripts, to make sure this runs everywhere. 
if test ! -f /lib/lsb/init-functions ; then 
    echo "Cannot start $NAME - LSB not supported"
    exit 1
fi
. /lib/lsb/init-functions

if test -f ${sysconfdir}/default/$NAME ; then 
    . ${sysconfdir}/default/$NAME
fi
if test -f ${sysconfdir}/sysconfig/$NAME ; then 
    . ${sysconfdir}/sysconfig/$NAME
fi


case "$1" in
    start)
	test "x$VERBOSE" != "xno" && log_daemon_msg "Starting $DESC" "$NAME"
	start_daemon -p $PIDFILE -- $DAEMON $DAEMON_OPTS > $LOGFILE 2>&1 &
	test "x$VERBOSE" != "xno" && log_end_msg $?
	;;
    stop)
	test "x$VERBOSE" != "xno" && log_daemon_msg "Stopping $DESC" "$NAME"
	killproc -p $PIDFILE -- $DAEMON $DAEMON_OPTS 
	test "x$VERBOSE" != "xno" && log_end_msg $?
	;;
    force-reload|restart)
	$0 stop
	$0 start
	;;
    *)
	echo "Usage: ${sysconfdir}/init.d/dim-dns {start|stop|restart|force-reload}" >&2 
	exit 1
	;;
esac

exit 0

#
# EOF
#
