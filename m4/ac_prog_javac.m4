dnl
dnl $Id: ac_prog_javac.m4,v 1.1 2012-03-07 08:27:44 cholm Exp $
dnl
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl ------------------------------------------------------------------
dnl
dnl Group of macro's to enable `normal' Java language support.
dnl
dnl 
dnl ------------------------------------------------------------------
dnl
dnl Set up Java variables. 
dnl
m4_ifndef([AC_LANG_DEFINE],[
m4_define([AC_LANG(Java)],
[
   ac_ext=java
   # ac_objext=class
   ac_cpp=true
   ac_compile='if test ! "x$CLASSPATH" = "x" ; then CLASSPATH=$CLASSPATH $JAVAC $JAVACFLAGS conftest.$ac_ext ; else $JAVAC $JAVACFLAGS conftest.$ac_ext ; fi ; if test conftest.class ; then mv conftest.class conftest.${ac_objext} ; fi >&AS_MESSAGE_LOG_FD'
   ac_link='$JAVAC $JAVACFLAGS conftest.$ac_ext >&AS_MESSAGE_LOG_FD'
   ac_compiler_gnu=$ac_cv_javac_compiler_gnu
])],[
 AC_LANG_DEFINE([Java],[java],[Java],[],[],
 [  ac_ext=java
    # ac_objext=class
    ac_cpp=true
    ac_compile='if test ! "x$CLASSPATH" = "x" ; then CLASSPATH=$CLASSPATH $JAVAC $JAVACFLAGS conftest.$ac_ext ; else $JAVAC $JAVACFLAGS conftest.$ac_ext ; fi ; if test conftest.class ; then mv conftest.class conftest.${ac_objext} ; fi >&AS_MESSAGE_LOG_FD'
    # ac_compile='if test ! "x$CLASSPATH" = "x" ; then CLASSPATH=$CLASSPATH $JAVAC $JAVACFLAGS conftest.$ac_ext ; else $JAVAC $JAVACFLAGS conftest.$ac_ext ; fi  >&AS_MESSAGE_LOG_FD'
    ac_link='$JAVAC $JAVACFLAGS conftest.$ac_ext >&AS_MESSAGE_LOG_FD'
    ac_compiler_gnu=$ac_cv_javac_compiler_gnu
 ])])

m4_define([AC_LANG_JAVA],          [AC_LANG(Java)])
m4_define([_AC_LANG_ABBREV(Java)], [java])
m4_define([_AC_LANG_PREFIX(Java)], [JAVA])

dnl ------------------------------------------------------------------
dnl
dnl Enables
dnl
dnl AC_LANG_PUSH(Java)
dnl AC_LANG_SOURCE([public class Test {}])
dnl AC_LANG_POP(Java)
dnl
m4_define([AC_LANG_CONFTEST(Java)],
[cat << _ACEOF > conftest.java
/* [#]line __oline__ "configure" */
$1
_ACEOF
])
m4_define([AC_LANG_SOURCE(Java)],
[_ACEOF
cat << _ACEOF > conftest.java
/* [#]line __oline__ "configure" */
$1])

dnl ------------------------------------------------------------------
dnl
dnl Enables
dnl
dnl AC_LANG_PUSH(Java)
dnl AC_LANG_PROGRAM([import package.*],[public class Test {}])
dnl AC_LANG_POP(Java)
dnl
m4_define([AC_LANG_PROGRAM(Java)],
[$1
public class conftest 
{
$2
}])
m4_copy([AC_LANG_PROGRAM(Java)],[AC_LANG_CALL(Java)])
m4_copy([AC_LANG_PROGRAM(Java)],[AC_LANG_FUNC_LINK_TRY(Java)])
m4_copy([AC_LANG_PROGRAM(Java)],[AC_LANG_BOOL_COMPILE_TRYE(Java)])


dnl ------------------------------------------------------------------
dnl
dnl Compiles and such
dnl 
m4_define([AC_LANG_PREPROC(Java)],  
	  [true
	   AC_PROVIDE([AC_LANG_PREPROC(Java)])])
m4_define([AC_LANG_COMPILER(Java)], 
	  [dnl AC_REQUIRE([AC_PROG_JAVAC])
	   AC_PROVIDE([AC_LANG_COMPILER(Java)])])

dnl ------------------------------------------------------------------
dnl
dnl Checks if we can find a Java byte-code compiler. 
dnl
AC_DEFUN([AC_PROG_JAVAC],
[
   AC_REQUIRE([AC_EXEEXT])dnl
   AC_LANG_PUSH(Java)dnl
   AC_ARG_VAR(JAVAC,[Java bytecode compiler])
   AC_ARG_VAR(JAVAFLAGS,[Java bytecode compiler flags])
   candidates="guavac javac jikes "
   m4_ifval([$1],
            [AC_CHECK_TOOLS(JAVAC, [$1])],
            [AC_CHECK_TOOL(JAVAC, "gcj${EXEEXT} -C")
             if test -z "$JAVAC" ; then 
               if test -n "${ac_tool_prefix}" ; then
		 for c in $candidates ; do 
	           AC_CHECK_PROG(JAVAC, [${ac_tool_prefix}${c}${EXEEXT}],
	                                [${ac_tool_prefix}${c}${EXEEXT}])
	           if test -n "$JAVAC" ; then break ; fi
                 done
               fi
               if test -z "$JAVAC" ; then 
	         for c in $candidates ; do 
	           AC_CHECK_PROG(JAVAC, [${c}${EXEEXT}],[${c}${EXEEXT}])
	           if test -n "$JAVAC" ; then break ; fi
                 done
	       fi
             else 
	        ac_cv_javac_compiler_gnu=$JAVAC
             fi])
   # test "x$JAVAC" = x && \
   #   AC_MSG_ERROR([no acceptable Java compiler found in \$PATH])
   if test "x$JAVAC" = "x" ; then
       ifelse([$3], , :, [$3])
   else
       AC_PROG_JAVAC_WORKS
   fi
   if test "x$ac_cv_prog_javac_works" = "xno" ; then
       ifelse([$3], , :, [$3])
   else 
       ifelse([$2], , :, [$2])
   fi
   AC_LANG_POP(Java)
   AC_PROVIDE([$0])dnl
])
dnl ------------------------------------------------------------------
dnl 
dnl Check if the Java compiler actually works. 
dnl
AC_DEFUN([AC_PROG_JAVAC_WORKS],
[
   AC_CACHE_CHECK([if $JAVAC works], ac_cv_prog_javac_works, [
     AC_COMPILE_IFELSE([AC_LANG_PROGRAM([],[])],
		       [ac_cv_prog_javac_works=yes],
                       [ac_cv_prog_javac_works=no])  
  
  ])
  AC_PROVIDE([$0])dnl
])

dnl ------------------------------------------------------------------
dnl 
dnl User-level macro to set the class path for Java. 
dnl
AC_DEFUN([AC_JAVA_CLASSPATH],
[
  AC_ARG_WITH([classpath],
	      [AC_HELP_STRING([--with-classpath=DIRS_OR_JARS],
                       [Set class path to directories and/or archives])],
              [if test "x$withval" = "x" ; then
	         :
               else
                 CLASSPATH="$withval:$CLASSPATH"
               fi])
  AC_ARG_VAR(CLASSPATH, 
             [Path of directories and/or archives to search for classes])
  AC_ARG_VAR(JAVACFLAGS,
             [Flags passed to the Java compiler])
  if test "x$CLASSPATH" = "x" ; then 
      :
  else 
      JAVACFLAGS="-classpath $CLASSPATH $JAVACFLAGS"
  fi
])

dnl
dnl EOF
dnl
