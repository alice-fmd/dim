dnl
dnl $Id: ac_labview.m4,v 1.1 2012-03-07 08:27:44 cholm Exp $
dnl
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl ------------------------------------------------------------------
dnl
dnl Macro to set LabView prefix 
dnl
AC_DEFUN([AC_PREFIX_LABVIEW],
[
    AC_ARG_WITH([labview-prefix],
                [AC_HELP_STRING([--with-labview-prefix],
                                [Where LabVIEW is installed])])
    AC_CACHE_CHECK([for LabVIEW installation prefix],[ac_cv_labview_prefix],
    [if test "x$with_labview_prefix" = "x" ; then 
	ac_cv_labview_prefix=$with_labview_prefix
     fi
     if test "x$ac_cv_labview_prefix" = "x" && \
       test -f /etc/natinst/labview/labview.dir ; then 
	ac_cv_labview_prefix=$(cat /etc/natinst/labview/labview.dir)
     fi])
    AC_PROVIDE([AC_PREFIX_LABVIEW])
])

dnl ------------------------------------------------------------------
dnl
dnl
dnl
AC_DEFUN([AC_PROG_LVSBUTIL],
[
    AC_REQUIRE([AC_PREFIX_LABVIEW])
    save_PATH=$PATH
    if test ! "x$ac_cv_labview_prefix" = "x" ; then 
	PATH=${ac_cv_labview_prefix}/cintools:$PATH
    fi
    AC_PATH_PROG(LVSBUTIL, [lvsbutil])
    PATH=$save_PATH
    AC_PROVIDE([AC_PROG_LVSBUTIL])
])

dnl ------------------------------------------------------------------
dnl
dnl
dnl
AC_DEFUN([AC_CHECK_LIBCIN],
[
    AC_REQUIRE([AC_PREFIX_LABVIEW])
    save_LDFLAGS="$LDFLAGS"
    ac_tmp_have_libcin=no
    if test ! "x$ac_cv_labview_prefix" = "x" ; then 
	LDFLAGS="-L${ac_cv_labview_prefix}/cintools $LDFLAGS"
    fi
    AC_CHECK_LIB(cin, CINLoad, [ac_tmp_have_libcin=yes])
    LDFLAGS=$save_LDFLAGS
    if test "x$ac_tmp_have_libcin" = "xyes" && \
       test ! "x$ac_cv_labview_prefix" = "x" ; then
	CIN_LDFLAGS="-L${ac_cv_labview_prefix}/cintools"
	CIN_LIBS="-lcin"
    fi
    AC_SUBST(CIN_LDFLAGS)
    AC_SUBST(CIN_LIBS)
    if test "x$ac_tmp_have_libcin" = "xyes" ; then 
        ifelse([$1], , :, [$1])
    else
        ifelse([$2], , :, [$2])
    fi
    AC_PROVIDE([AC_CHECK_LIBCIN])
])

dnl ------------------------------------------------------------------
dnl
dnl
dnl
AC_DEFUN([AC_CHECK_CIN_O],
[
    AC_REQUIRE([AC_PREFIX_LABVIEW])
    save_LDFLAGS="$LDFLAGS"
    ac_tmp_have_cin_o=no
    AC_CHECK_FILE($ac_cv_labview_prefix/cintools/cin.o,[ac_tmp_have_cin_o=yes])
    if test "x$ac_tmp_have_cin_o" = "xyes" && \
       test ! "x$ac_cv_labview_prefix" = "x" ; then
	CIN_O=$ac_cv_labview_prefix/cintools/cin.o
    fi
    AC_SUBST(CIN_O)
    if test "x$ac_tmp_have_cin_o" = "xyes" ; then 
        ifelse([$1], , :, [$1])
    else
        ifelse([$2], , :, [$2])
    fi
    AC_PROVIDE([AC_CHECK_CIN_O])
])
	  
dnl ------------------------------------------------------------------
dnl
dnl
dnl
AC_DEFUN([AC_CHECK_CIN_EXTCODE],
[
    AC_REQUIRE([AC_PREFIX_LABVIEW])
    save_CPPFLAGS="$CPPFLAGS"
    ac_tmp_have_cin_extcode=no
    if test ! "x$ac_cv_labview_prefix" = "x" ; then 
	CPPFLAGS="-I${ac_cv_labview_prefix}/cintools $CPPFLAGS"
    fi
    AC_CHECK_HEADER(extcode.h,[ac_tmp_have_cin_extcode=yes])
    if test "x$ac_tmp_have_cin_ext_code" = "xyes" ; then
	AC_MSG_CHECKING(if extcode.h defines CIN_VERS)
	AC_COMPILE_IFELSE([AC_LANG_PROGRAM([#include <extcode.h>
],
[#ifndef CIN_VERS
choke me
#endif
])], , [ac_tmp_have_cin_ext_code=no])
	AC_MSG_RESULT($ac_tmp_have_cin_ext_code)
    fi
    CPPFLAGS=$save_CPPFLAGS
    if test "x$ac_tmp_have_cin_extcode" = "xyes" && \
       test ! "x$ac_cv_labview_prefix" = "x" ; then
	CIN_CPPFLAGS="-I${ac_cv_labview_prefix}/cintools"
    fi
    AC_SUBST(CIN_CPPFLAGS)
    if test "x$ac_tmp_have_cin_extcode" = "xyes" ; then 
        ifelse([$1], , :, [$1])
    else
        ifelse([$2], , :, [$2])
    fi
    AC_PROVIDE([AC_CHECK_CIN_EXTCODE])
])

dnl ------------------------------------------------------------------
dnl
dnl
dnl
AC_DEFUN([AC_LABVIEW_CIN],
[
    ac_tmp_have_labview_cin=yes
    AC_REQUIRE([AC_PREFIX_LABVIEW])
    AC_REQUIRE([AC_CHECK_CIN_EXTCODE],[],[ac_tmp_have_labview_cin=no])
    AC_REQUIRE([AC_CHECK_CIN_O],[],[ac_tmp_have_labview_cin=no])
    AC_REQUIRE([AC_CHECK_LIBCIN],[],[ac_tmp_have_labview_cin=no])
    AC_REQUIRE([AC_PROG_LVSBUTIL],[],[ac_tmp_have_labview_cin=no])
    if test "x$ac_tmp_have_labview_cin" = "xyes" ; then 
        ifelse([$1], , :, [$1])
    else
        ifelse([$2], , :, [$2])
    fi
])	  
	        
#
# EOF
#
