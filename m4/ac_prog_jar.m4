dnl
dnl $Id: ac_prog_jar.m4,v 1.1 2012-03-07 08:27:44 cholm Exp $
dnl
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl ------------------------------------------------------------------
dnl
dnl Search for Java archiver
dnl
AC_DEFUN([AC_PROG_JAR],
[
  AC_PATH_PROGS(JAR, [jar fastjar])
  AC_ARG_VAR(JAR, [Java object archiver])
])

dnl
dnl EOF
dnl
