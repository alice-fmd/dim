dnl
dnl $Id: ac_java_jni.m4,v 1.1 2012-03-07 08:27:44 cholm Exp $
dnl
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or
dnl  modify it under the terms of the GNU Lesser General Public License
dnl  as published by the Free Software Foundation; either version 2.1
dnl  of the License, or (at your option) any later version.
dnl
dnl  This library is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl  Lesser General Public License for more details.
dnl
dnl  You should have received a copy of the GNU Lesser General Public
dnl  License along with this library; if not, write to the Free
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
dnl  02111-1307 USA
dnl
dnl ------------------------------------------------------------------
dnl
dnl Check for JNI include path
dnl
AC_DEFUN([AC_JAVA_JNI],
[
   AC_REQUIRE([AC_PROG_JAVAC])
   AC_REQUIRE([AC_PROG_JAVA])
   AC_MSG_CHECKING([for JRE home])
   AC_LANG_PUSH(Java)
   AC_COMPILE_IFELSE([AC_LANG_PROGRAM([],
                     [public static void main(String[[]] a) { 
		        System.out.println(System.getProperty("java.home"));
			}])],
		     [cp conftest.o conftest.class
		      JREHOME=`${JAVA} conftest`
		      rm -f conftest.class])
   AC_LANG_POP(Java)
   AC_MSG_RESULT($JREHOME)		    

   have_jni_h=0
   if test "x$JREHOME" != "x" ; then 
     	base=`dirname $JREHOME` 
	save_CPPFLAGS=$CPPFLAGS 
	CPPFLAGS="$CPPFLAGS -I$base/include"
	AC_LANG_PUSH(C++)
	AC_CHECK_HEADER([jni.h])
	AC_LANG_POP(C++)
	if test "x$ac_cv_header_jni_h" = "xno" ; then 
	   have_jni_h=0
	   CPPFLAGS=$save_CPPFLAGS
	else
	   have_jni_h=1
	fi
   fi
   if test $have_jni_h -lt 1 ; then
       ifelse([$2], , :, [$2])
   else 
       ifelse([$1], , :, [$1])
   fi	
])
dnl
dnl EOF
dnl
