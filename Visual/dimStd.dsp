# Microsoft Developer Studio Project File - Name="dimStd" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=dimStd - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "dimStd.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "dimStd.mak" CFG="dimStd - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "dimStd - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "dimStd - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "dimStd - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../bin"
# PROP Intermediate_Dir "../tmp"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
F90=df.exe
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "DIMSTD_EXPORTS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /Gz /MDd /W3 /Gm /GX /ZI /Od /I "../dim" /D "WIN32" /D PROTOCOL=1 /D "MIPSEL" /D "STDCALL" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "DIMSTD_EXPORTS" /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib wsock32.lib /nologo /dll /pdb:none /machine:I386

!ELSEIF  "$(CFG)" == "dimStd - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "../bin"
# PROP Intermediate_Dir "../tmp"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
F90=df.exe
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "DIMSTD_EXPORTS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /Gz /MDd /w /W0 /Gm /GX /ZI /Od /I "../dim" /D "WIN32" /D PROTOCOL=1 /D "MIPSEL" /D "STDCALL" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "DIMSTD_EXPORTS" /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib wsock32.lib /nologo /dll /debug /machine:I386
# SUBTRACT LINK32 /profile /incremental:no

!ENDIF 

# Begin Target

# Name "dimStd - Win32 Release"
# Name "dimStd - Win32 Debug"
# Begin Source File

SOURCE=..\src\conn_handler.c
# End Source File
# Begin Source File

SOURCE=..\src\copy_swap.c
# End Source File
# Begin Source File

SOURCE=..\src\dic.c
# End Source File
# Begin Source File

SOURCE=..\src\diccpp.cxx
# End Source File
# Begin Source File

SOURCE=..\src\dim_thr.c
# End Source File
# Begin Source File

SOURCE=..\src\dimcpp.cxx
# End Source File
# Begin Source File

SOURCE=..\src\dis.c
# End Source File
# Begin Source File

SOURCE=..\src\discpp.cxx
# End Source File
# Begin Source File

SOURCE=..\src\dll.c
# End Source File
# Begin Source File

SOURCE=..\src\dna.c
# End Source File
# Begin Source File

SOURCE=..\src\dtq.c
# End Source File
# Begin Source File

SOURCE=..\src\hash.c
# End Source File
# Begin Source File

SOURCE=..\src\open_dns.c
# End Source File
# Begin Source File

SOURCE=..\src\sll.c
# End Source File
# Begin Source File

SOURCE=..\src\swap.c
# End Source File
# Begin Source File

SOURCE=..\src\tcpip.c
# End Source File
# Begin Source File

SOURCE=..\src\tokenstring.cxx
# End Source File
# Begin Source File

SOURCE=..\src\utilities.c
# End Source File
# End Target
# End Project
